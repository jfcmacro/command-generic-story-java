package com.epam.rd.cls;

public interface Command<R> {
    R execute();
}
