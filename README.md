---

# Command Generic Story

**Estimated reading time**: 30 minutes

## Story Outline

Programming languages were defined to help us turn a value into another value. The transformation is described as a well-formed complete program. That program is defined as a static program, which means if the computation is changed, the program must be rewritten to fit the new computation. This is common in almost all programming languages that belong to the Imperative Paradigm; the program must be rewritten. But, in paradigm declarative, the program is seen as data (a value) and can be transformed and rewritten dynamically as another computation.

Object-Oriented Programming, particularly Java, is an Imperative Programming Language, meaning that the computation described in a
source code is defined statically. Suppose you need to describe a calculation dynamically. In that case, you can use the Command Design Pattern: “Encapsulate a request as an object, thereby letting you parameterize clients with different request, queue or log request, and support undoable operations” [Gamma et al.,1995]. The Command Pattern can be used as parameterized computation, enabling us to dynamically change computation without recompiling the application.

In this story, we will use the Command Pattern to represent a dynamic computation through an application that implements the main functions of a database based on spreadsheets through the following types of computation: `forEach`, `filter`, `map`,  and `reduce`. This service will be implemented by starting with the design pattern called Command, as we already said it. In its first version, in a non-generic way, and the second we will improve it with
generic data type mechanisms.

## Story Organization
**Story Branch**: main

&gt; `git checkout main`

**Practical task tag for self-study**: task

&gt; `git checkout task`

Tags: #command_design_pattern, #generics


